module.exports = {
  purge: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        transparent: 'transparent',
        current: 'current',
        white: '#ffffff',
        black: '#000000',
        secondary: '#ece9e7',
        brown: {
          DEFAULT: '#534435',
          dark: '#937323',
        },
        green: {
          DEFAULT: '#6a974c',
          dark: '#007e50',
        },
        blue: {
          DEFAULT: '#00a9be',
          dark: '#bad5e5',
        },
        gray: {
          DEFAULT: '#4F4F4F',
        },
      },
      fontFamily: {
        heebo: ['Heebo', 'sans-serif'],
        zilla: ['Zilla Slab', 'serif'],
      },
      fontSize: {
        '80': '80px'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
